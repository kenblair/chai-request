'use strict';

function init(chai, utils) {

    /**
     * Set the specified flag to true.  Returns true if the flag is NOT already set.  This is used as a simple gate keeper
     * to easily run code only once per chain.
     *
     *    // checkFlag returns true because 'my.flag' wasn't set prior to this call
     *    if (utils.checkFlag(this, 'my.flag')) {
     *        // perform code once -- next time it will return false and skip this part
     *    }
     *
     * @param self The assertion to check the flag on.
     * @param flag The flag to check.
     * @returns {boolean} True if the flag was not yet set, false if the flag was already set.
     */
    utils.checkFlag = function checkFlag(self, flag) {
        if (!utils.flag(self, flag)) {
            utils.flag(self, flag, true);
            return true;
        }
        return false;
    };

    // todo: assertion docs
    // body
    chai.use(require('./assertions/body'));

    // json
    chai.use(require('./assertions/json'));

    // status/statusCode
    chai.use(require('./assertions/status'));

    // header/headers
    chai.use(require('./assertions/header'));

    // schema
    chai.use(require('./assertions/schema'));

    // cookies
    chai.use(require('./assertions/cookie'));
}

let request = require('./request');

exports = module.exports = {
    init: init,
    get: request.get,
    put: request.put,
    post: request.post,
    del: request.del,
    defaults: request.defaults,
    connect: request.connect
};