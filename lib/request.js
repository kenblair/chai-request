'use strict';

let request = require('request');

function execute(options) {
    return new Promise(function (resolve, reject) {
        if (options['enableCookies']) {
            let cookieJar = request.jar();
            request(Object.assign(options, {jar: cookieJar}), handleResponse(resolve, reject, cookieJar, options));
        }
        request(options, handleResponse(resolve, reject));
    });
}

function handleResponse(resolve, reject, cookieJar, options) {
    return function (error, response, body) {
        let result = {error: error, response: response, body: body};
        if (cookieJar) {
            cookieJar.getCookiesSync(options.url);
        }
        if (error) {
            reject(error);
        } else {
            resolve(result);
        }
    };
}

function buildOptions(method, url, body, options) {
    return Object.assign({
        method: method,
        url: url,
        json: true,
        body: body
    }, options);
}

function get(url, options) {
    let promise = execute(buildOptions('GET', url, undefined, options));
    promise.printResponse = function () {
        return promise.then(function (result) {
            console.log(result.response.toJSON());
            return result;
        });
    };
    promise.printBody = function () {
        return promise.then(function (result) {
            console.log(result.body);
            return result;
        });
    };
    return promise;
}

function post(url, body, options) {
    return execute(buildOptions('POST', url, body, options));
}

function put(url, body, options) {
    return execute(buildOptions('PUT', url, body, options));
}

function del(url, options) {
    return execute(buildOptions('DELETE', url, undefined, options));
}

function head(url, options) {
    return execute(buildOptions('HEAD', url, undefined, options));
}

function setDefaults(options) {
    request = request.defaults(options);
}

function connect(app) {
    let url = 'http://localhost:' + app.get('port') + '/';
    if (app.get('path')) {
        url += app.get('path');
    }
    setDefaults({
        baseUrl: url,
        json: true
    });
}

module.exports = {
    send: execute,
    get: get,
    post: post,
    put: put,
    del: del,
    head: head,
    defaults: setDefaults,
    connect: connect
};