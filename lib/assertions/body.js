'use strict';

exports = module.exports = function (chai, utils) {
    function body() {
        if (utils.checkFlag(this, 'body.chain')) {
            let value = utils.flag(this, 'object');
            utils.flag(this, 'object', value.body);

        }
    }

    chai.Assertion.addProperty('body', body);
};
