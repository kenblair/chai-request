'use strict';

const tv4 = require('tv4');

exports = module.exports = function (chai, utils) {
    let Assertion = chai.Assertion;

    function schema(jsonSchema) {
        var negate = utils.flag(this, 'negate') || false;
        this.assert(
            tv4.validate(this._obj, jsonSchema),
            'expected body to match schema' + '\n  ' + tv4.error,
            'expected body to not match schema'
        );
    }

    Assertion.addMethod('schema', schema);
};