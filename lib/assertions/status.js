'use strict';

exports = module.exports = function (chai, utils) {
    function status(statusCode, message) {
        if (statusCode) {
            this.to.equal(statusCode, message);
        } else {
            // todo: better error message
            throw new chai.AssertionError('Assertion used as a method but no argument provided.');
        }
    }

    function statusChain() {
        if (!utils.flag(this, 'status.chain')) {
            if (this._obj.response.status) {
                utils.flag(this, 'object', this._obj.response.status);
            } else if (this._obj.response.statusCode) {
                utils.flag(this, 'object', this._obj.response.statusCode);
            } else {
                // todo: better error message
                throw new chai.AssertionError('No status or statusCode found on response.');
            }
            utils.flag(this, 'status.chain', true);
        }
    }

    chai.Assertion.addChainableMethod('status', status, statusChain);
    chai.Assertion.addChainableMethod('statusCode', status, statusChain);
};