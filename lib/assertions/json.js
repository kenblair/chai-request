'use strict';

exports = module.exports = function (chai, utils) {

    function json() {

        let args = Array.prototype.slice.call(arguments);

        // original value
        let value = utils.flag(this, 'object');

        // if they provide a path then switch to the value at that path
        if (args.length > 0 && typeof args[0] === 'string') {
            value = utils.getPathValue(args[0], value);
        }

        utils.flag(this, 'object', value);

        if (args.length > 1) {
            args = args.slice(1);
            if ('string' === typeof args[0]) {
                this.to.be.string(args[0]);
            } else if (args[0] instanceof RegExp) {
                this.to.match(args[0]);
            } else if ('function' === typeof args[0]) {
                args[0](utils.flag(this, 'object'));
            } else if (Array.isArray(args[0])) {
                if ('object' === utils.flag(this, 'object')) {
                    this.to.be.keys(args[0]);
                } else if (Array.isArray(utils.flag(this, 'object'))) {
                    this.to.be.members(args[0]);
                }
            }
        }
    }

    chai.Assertion.addMethod('json', json);
};