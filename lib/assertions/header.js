'use strict';

exports = module.exports = function (chai, utils) {
    function header(key, value, message) {
        if (typeof key === 'string') {
            utils.flag(this, 'object', this._obj[key]);
            this.assert(
                this._obj !== undefined,
                'expected #{exp} to exist',
                'expected #{exp} to not exist',
                key
            );
        } else {
            throw new chai.AssertionError('Header key must be a String, found: ' + typeof key);
        }

        if ('string' === typeof value) {
            this.to.equal(value, message);
        } else if (value instanceof RegExp) {
            this.to.match(value, message);
        } else if (value !== undefined) {
            throw new chai.AssertionError('Value must be a String or RegExp, found: ' + typeof value);
        }
    }

    function headerChain() {
        if (utils.checkFlag(this, 'header.chain')) {
            utils.flag(this, 'object', this._obj.response.headers);
        }
    }

    chai.Assertion.addChainableMethod('header', header, headerChain);
    chai.Assertion.addChainableMethod('headers', header, headerChain);
};