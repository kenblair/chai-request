'use strict';

const toughCookie = require('tough-cookie').Cookie;

exports = module.exports = function (chai, utils) {

    function cookie(key, value, message) {
        if (typeof key === 'string') {
            // if they specify a key then set the target to that specific cookie
            let found = false;
            this._obj.forEach(cookie => {
                if (cookie.key === key) {
                    found = true;
                    utils.flag(this, 'object', cookie.value);
                }
            });
            if (!found) {
                utils.flag(this, 'object', undefined);
            }
        } else {
            throw new chai.AssertionError('Header key must be a String, found: ' + typeof key);
        }

        if (arguments.length === 1) {
            // assert on existence if no matcher was provided
            this.assert(
                this._obj !== undefined,
                'expected #{exp} to exist',
                'expected #{exp} to not exist',
                key
            );
        }

        if (typeof value === 'string') {
            this.to.equal(value, message);
        } else if (value instanceof RegExp) {
            this.to.match(value, message);
        } else if (value !== undefined) {
            throw new chai.AssertionError('Value must be a String or RegExp, found: ' + typeof value);
        }
    }

    function cookieChain() {
        // parse cookies and make them the target using the response header
        // does not yet support running within browser
        if (utils.checkFlag(this, 'cookie.chain')) {
            let header = this._obj.response.headers['set-cookie'];
            if (Array.isArray(header)) {
                utils.flag(this, 'object', header.map(toughCookie.parse));
            } else {
                utils.flag(this, 'object', [toughCookie.parse(header)]);
            }
        }
    }

    chai.Assertion.addChainableMethod('cookie', cookie, cookieChain);
    chai.Assertion.addChainableMethod('cookies', cookie, cookieChain);
};