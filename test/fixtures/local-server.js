'use strict';

const express = require('express')();
const Q = require('q');

express.use(require('body-parser').json());

express.all('/', send);
express.all('/get', send);
express.all('/post', send);
express.all('/put', send);

function send(req, res) {
    res.send({
        args: req.params,
        headers: req.headers,
        origin: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
        url: req.protocol + '://' + req.get('host') + req.originalUrl
    });
}

before(function () {
    global.baseUrl = 'http://127.0.0.1:' + express.listen(0).address().port;
});