'use strict';

describe('json plugin', function () {

    let result = {
        body: {
            array: [1,2,3,4,5],
            foo: 'foobar',
            nested: {
                array: [1,2,3,4,5],
                foo: 'another foobar'
            }
        }
    };

    it('should work with both Chai styles', function () {
        result.should.have.json('body.foo', 'foobar');
        expect(result).to.have.json('body.foo', 'foobar');
    });

    it('should work with the body property', function() {
        result.should.have.body.with.json('foo', 'foobar');
    });

    it('should assert on a top level property', function () {
        result.should.have.body.with.json('nested').that.eql(result.body.nested);
        expect(function () {
            result.should.not.have.body.with.json('nested').that.eql(result.body.nested);
        }).to.throw(chai.AssertionError, /not deeply equal/);
    });

    it('should assert on a path using dot notation', function () {
        result.should.have.body.with.json('nested.array').that.eql(result.body.nested.array);
        expect(function () {
            result.should.not.have.body.with.json('nested.array').that.eql(result.body.nested.array);
        }).to.throw(chai.AssertionError, /not deeply equal/);
    });

    it('should work with chained assertions', function () {
        result.should.have.body.with.json('array').with.length.of.at.least(5);
        expect(function () {
            result.should.not.have.body.with.json('array').with.length.above(4);
        }).to.throw(chai.AssertionError, /not have a length above 4/);
    });

});