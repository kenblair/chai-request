'use strict';

describe('header plugin', function () {

    let result = {
        response: {
            headers: {
                'Content-Type': 'application/json',
                'Host': 'foo.bar'
            }
        }
    };

    it('should assert on a header', function () {
        result.should.have.header('Content-Type').that.equals('application/json');

        expect(function () {
            result.should.not.have.header('Content-Type').that.equals('application/json');
        });
    });
});