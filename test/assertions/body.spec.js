'use strict';

describe('body plugin', function () {

    let result = {
        body: {
            args: {},
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Foobar': 'foobar'
            }
        }
    };

    it('should work with both Chai styles', function () {
        result.should.have.body.that.is.an('object');
        expect(result).to.have.body.that.is.an('object');
        expect(function () {
            result.should.not.have.body.that.is.an('object');
        }).to.throw(chai.AssertionError, /not to be an object/);
        expect(function () {
            expect(result).to.not.have.body.that.is.an('object');
        }).to.throw(chai.AssertionError, /not to be an object/);
    });

    it('should assert against the body', function () {
        result.should.have.body.that.eql(result.body);
        expect(function () {
            result.should.not.have.body.that.eql(result.body);
        }).to.throw(chai.AssertionError, /not deeply equal/);
    });

    it('should work with chained property assertions', function () {
        result.should.have.body.with.property('headers').with.property('Foobar', 'foobar');
        let rootArray = { body: [1,2,3,4,5] };
        rootArray.should.have.body.with.lengthOf(5);
        rootArray.should.have.body.with.length.at.least(4);
        expect(function () {
            rootArray.should.have.body.with.length.not.at.least(4);
        }).to.throw(chai.AssertionError, /to have a length below 4/);
    });
});