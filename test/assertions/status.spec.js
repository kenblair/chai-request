'use strict';

describe('status plugin', function () {
    let result = {
        response: {
            status: 200
        }
    };

    it('should assert against response status', function () {
        result.should.have.status(200);
        expect(result).to.have.status(200);
        expect(function () {
            result.should.not.have.status(200);
        }).to.throw(chai.AssertionError, /not equal/);
        expect(function () {
            expect(result).to.not.have.status(200);
        }).to.throw(chai.AssertionError, /not equal/);
    });
});