'use strict';

describe('cookie plugin', function () {

    let result = {
        response: {
            headers: {
                'set-cookie': ['cookie1=value1;httpOnly;secure', 'cookie2=value2;httpOnly', 'cookie3=value3']
            }
        }
    };

    it('should assert on a cookie', function () {
        result.should.have.cookie('cookie1', 'value1');
        result.should.not.have.cookie('cookie2', 'value3');
        result.should.have.cookie('cookie1').that.matches(/value./);

        expect(function () {
            result.should.not.have.cookie('cookie1');
        });
    });
});