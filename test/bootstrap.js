'use strict';

const request = require('../lib/chai-request');
const chai = require('chai');

chai.should();
chai.use(request.init);

global.chai = chai;
global.expect = chai.expect;
global.request = request;

global.inBrowser = false;
if (typeof document !== 'undefined') {
    global.inBrowser = true;
}