'use strict';

describe('httpbin API', function () {
    describe('should work with all plugins', function () {
        this.timeout(10000);

        let httpbinSchema = {
            type: 'object',
            properties: ['args', 'headers', 'origin', 'url'],
            required: ['args', 'headers', 'origin', 'url']
        };

        let arraySchema = {
            type: 'array',
            minItems: '1'
        };

        it('status', function () {
            return request.get('http://httpbin.org/get')
                .then(res => {
                    res.should.have.status(200);
                    res.should.have.status.that.equals(200);
                });
        });
        it('body', function () {
            return request.post('http://httpbin.org/post', {foo: 'bar'})
                .then(res => {
                    res.should.have.body.with.property('json');
                    // res.should.have.body.with.json('json', '')
                });
        });
        it('header', function () {
            return request.get('http://httpbin.org/headers')
                .then(res => {
                    res.should.have.header('content-type', 'application/json');
                    res.should.not.have.header('not-a-header');
                });
        });

        it('schema', function () {
            return request.get('http://httpbin.org/get')
                .then(res => {
                    res.should.have.body.with.schema(httpbinSchema);
                    res.should.not.have.body.with.schema(arraySchema);
                });
        });

        it('cookie', function () {
            if (inBrowser) {
                return this.skip();
            }
            return request.get('http://httpbin.org/response-headers?set-cookie=cookie1=value1&set-cookie=cookie2=value2&set-cookie=cookie3=value3')
                .then(res => {
                    res.should.have.cookie('cookie1');
                    res.should.not.have.cookie('cookie4');

                    res.should.have.cookie('cookie1', 'value1');
                    res.should.not.have.cookie('cookie1', 'value2');

                    res.should.have.cookies.with.lengthOf(3);
                });
        })
    });
});